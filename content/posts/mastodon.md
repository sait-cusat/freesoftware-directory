---
title: "Mastodon"
date: 2022-11-20T09:03:20-08:00
draft: false
---
## Introduction

Mastodon is a free and open-source software for running self-hosted social networking services.[a] It has microblogging features similar to Twitter, which are offered by a large number of independently run nodes, known as instances or servers, each with its own code of conduct, terms of service, privacy policy, privacy options, and content moderation policies.[6][7][8]

Each user is a member of a specific Mastodon server that can interact seamlessly with users in any other server. This is intended to give users the flexibility to select a server whose policies they prefer, but keep access to a larger federated social network. Mastodon is powered by the ActivityPub protocol, making it part of the Fediverse ensemble of services such as Lemmy, Pixelfed, Friendica, and PeerTube.

Mastodon was created by Eugen Rochko and announced on Hacker News in October 2016,[9] and gained significant adoption in 2022 in the wake of Twitter's acquisition by Elon Musk.[10][11][12]

The project is maintained by German non-profit Mastodon gGmbH.[13] Mastodon development is crowdfunded and the code does not support advertisement.



 
