---
title: "Okular"
date: 2022-11-20T09:03:20-08:00
draft: false
---

## Introduction

Okular is a multi-platform document viewer developed by the KDE community and based on Qt and KDE Frameworks libraries . It is distributed as part of the KDE Applications bundle . Okular supports many formats, including PDF, EPub, DjVU, and MD for documents; JPEG, PNG, GIF, Tiff, WebP for images; CBR and CBZ for comics; and many more .
It has a wide range of features that lets you easily manage your documents. You can annotate your PDFs with Okular’s ‘Annotation mode’, add inline and popup notes, highlight and underline text, or even add your own text. With Okular’s ‘Selection mode’, you can copy and paste almost anything from your documents to elsewhere. And, for those times the text you want to read is too small, you can use the ‘Magnifier mode’ . Okular also allows you to view and verify digital signatures embedded in PDFs, check if they are still valid, and detect any modifications since the moment the document was signed. You can even sign PDFs yourself! 1. Okular is released under the GPLv2+, and is thus fully Free Software 1. It is also the very first software product to receive the Blue Angel ecolabel for being resource and energy efficient 1.
Okular works on multiple platforms, including but not limited to Linux, Windows, macOS, \*BSD, etc. The last stable release is Okular 20.12.
