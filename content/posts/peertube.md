---
title: "peer tube"
date: 2023-10-2023:03:20-08:00
draft: false
---
## introduction

PeerTube was created by a web developer known as Chocobozzz as a peer-to-peer alternative to YouTube, utilizing the WebTorrent protocol to share videos.[9] He was contacted in 2017 by Framasoft, which had a campaign called Contributopia,[10] the goal of which is to create alternatives to centralized platforms. In order to support him and his work, notably on improving the design and usability, Framasoft hired the developer

