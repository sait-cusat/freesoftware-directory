---
title: "gnote"
date: 2023-10-07T09:03:20-08:00
draft: false
---
## Introduction

Gnote is a free and open-source desktop note-taking application written for Linux, cloned by Hubert Figuière from Tomboy.[2] It uses a Wiki-like linking system to connect notes together. Gnote is part of the GNOME desktop environment, often filling the need for personal information management. The main principle is a notepad with a wiki-style interface. Words in the note body that match existing note titles automatically become hyperlinks, allowing for the management of large libraries of personal information, such as references to favorite artists that would then automatically be highlighted in notes containing their names. Plugins extend the program to include functionality like exporting to HTML and printing support. As of version 0.8.0, Gnote has been ported to GTK+3.
